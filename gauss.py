import numpy as np


def gauss(matrix, vector):
    matrix_copy = matrix.copy()
    vector_copy = vector.copy()

    matrix_copy = np.column_stack((matrix_copy, vector_copy))

    m = matrix_copy.shape[0]
    n = matrix_copy.shape[1]

    k = 0

    free_var = np.zeros(n - 1, dtype=np.int)
    not_free_var = np.zeros(n - 1, dtype=np.int)

    free_var_count = 0
    not_free_var_count = 0

    for j in range(n - 1):
        index = -1
        for i in range(k, m):
            if (matrix_copy[i, j] != 0):
                index = i
        if index != -1:
            matrix_copy[k, :], matrix_copy[index, :] = matrix_copy[index, :].copy(), matrix_copy[k, :].copy()
            value = matrix_copy[k, j]
            for t in range(j, n):
                matrix_copy[k, t] /= value
            for t in range(m):
                if t == k:
                    continue
                matrix_copy[t, :] -= matrix_copy[t, j] * matrix_copy[k, :]
            not_free_var[not_free_var_count] = j
            not_free_var_count += 1
            k += 1
        else:
            free_var[free_var_count] = j
            free_var_count += 1

    X = np.zeros(n - 1)

    for i in range(k, m):
        if matrix_copy[i, n - 1] != 0:
            return "No solutions"

    if (free_var_count == 0):
        for j in range(n - 1):
            X[j] = matrix_copy[j, n - 1]
        return X

    return "Infinity solutions"
    
    
# Test

matrix = numpy.array([[1., 2., 3., -2.],
                [2., 4., -2., -2.],
                [3., 2., -1., 2.],
                 [2, -3., 2., 1.]])
vector = numpy.array([6.,18.,5.,-8.])

print(Gauss.gauss(matrix, vector))