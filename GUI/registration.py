import PySimpleGUI as sg

sg.theme('Dark Purple 5')

def register():
    layout = [[sg.Text('Enter your name to register')],
              [sg.InputText(key='username')],
              [sg.Submit(), sg.Cancel()]]

    window = sg.Window('Registration', layout)

    event, values = window.read()
    window.close()

    username : str = values['username']
    
    return username 



#sg.popup('You entered', text_input)