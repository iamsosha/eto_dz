class Element:  # элементы и обратные к ним

    def __init__(self, symbol: chr, isInv: bool):
        self.symbol = symbol
        self.isInv = False

    def isSelfDestruct(self, elem):
        # аннигиляция обратных элементов при встрече
        return self.symbol == elem.symbol and self.isInv != elem.isInv


class Free:  # группа, свободная от предрассудков ;)

    def __init__(self, group: [Element]):
        self.group: [Element] = group  # элементы
        self.neutral = ''  # нейтральный элемент

    def ConcatSymbol(self, elem: Element, side: bool):
        # оконкатенация букв
        # дополнительно определяем, с какой стороны конкатенируем
        if side:
            way = -1
        else:
            way = 0
        if len(self.group) == 0:
            self.group.append(elem)
        elif self.group[way].isSelfDestruct(elem):
            self.group.remove(self.group[way])
        else:
            if side:
                self.group.append(elem)
            else:
                self.group = [elem] + self.group

    def Concat(self, Group: Free, side: bool):  # конкатенация двух слов
        if side == False:
            Group.group.reverse()
        for elem in Group.group:
            self.ConcatSymbol(elem, side)
        if side == False:
            Group.group.reverse()

    def Print(self):  # выводим, что получилось
        for elem in self.group:
            print(elem.symbol, end='')


a = Free([Element('c', True), Element('a', True)])
b = Free([Element('A', False), Element('c', False)])
a.Concat(b, False)
a.Print()


