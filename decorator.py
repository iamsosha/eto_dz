def love_n(n):
    def logged(genuine_function):
        def fake_function(a): 
            result = a
            for i in range(0, n):
                result = genuine_function(result)
            return result
        return fake_function
    return logged
    
@love_n(3)
def test_function(n):
    return pow(n, 2/3)

print(test_function(5000))