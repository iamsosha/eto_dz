def prefix(s):
    slen = len(s)
    result = [0]
    for i in range(1, slen):
        z = result[i-1]
        while z != 0 and s[i] != s[z]:
            z = result[z-1]
        if s[i] == s[z]:
            z += 1
        result.append(z)
    return result

def search(source, pattern):
    result = []
    pattern_length = len(pattern)
    z_func = prefix(pattern+'$'+source)
    for i in range(pattern_length+1,len(z_func)):
        if z_func[i] == pattern_length:
            result.append(i - 2*pattern_length)
    return result


source = input()
pattern = input()

print(len(search(source, pattern)))

