source = input()
pattern = input()
ascii_size = 256
prime_number = 282589933

def Hash(string):
    res = 0
    length = len(string)
    for i in range(length):
        res = (ascii_size * res + ord(string[i])) % prime_number
    return res


def Search(source, pattern):
    result = []
    pattern_hash = Hash(pattern)
    pattern_len = len(pattern)
    for i in range(len(source)-pattern_len + 1):
        if (Hash(source[i:i+pattern_len]) == pattern_hash):
            if (source[i:i+pattern_len] == pattern):
                result.append(i)
    return result


print(len(Search(source, pattern)))
